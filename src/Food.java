import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.transform.Affine;
import javafx.scene.transform.Rotate;

public class Food {
    public Coordinate foodPosition;
    FoodType foodType;
    Canvas canvas;
    GraphicsContext gc;
    Rotate rotate;
    public Food(Canvas canvas, Coordinate foodPosition, FoodType foodType){
        this.canvas = canvas;
        this.foodPosition = foodPosition;
        this.foodType = foodType;
        gc = canvas.getGraphicsContext2D();

    }

    public void drawFood(){
        //Set the centre of rotation. Needs to use updated position




        if(foodType == FoodType.GROWTAIL){
            gc.setFill(Color.BLACK);
            gc.setStroke(Color.BLACK);
        }else if(foodType == FoodType.GROWHEAD){
            gc.setFill(Color.AQUA);
            gc.setStroke(Color.AQUA);
        }else if(foodType == FoodType.SPEEDUP){
            gc.setFill(Color.BLUE);
            gc.setStroke(Color.BLUE);
        }

        gc.strokeRect(foodPosition.getxCoord(), foodPosition.getyCoord(), Coordinate.unitSize, Coordinate.unitSize);
        //gc.strokeOval(foodPosition.getxCoord(), foodPosition.getyCoord(), Coordinate.unitSize, Coordinate.unitSize);

    }

    public Coordinate getPosition(){
        return foodPosition;
    }

    public void drawLifeOfFood(double percentage){

        gc.setGlobalAlpha(1-percentage);
        gc.fillRect(foodPosition.getxCoord(), foodPosition.getyCoord(), Coordinate.unitSize, Coordinate.unitSize);
        gc.setGlobalAlpha(1);
    }




}
