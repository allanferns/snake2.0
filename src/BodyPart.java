import javafx.animation.*;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class BodyPart {
    private Canvas canvas;
    private GraphicsContext gc;
    public boolean isHead;
    private int length;
    private Coordinate currentPosition;
    boolean isBig;



    public BodyPart(Canvas canvas, Coordinate coordinate, int length, boolean isHead){
        //Initialisations
        this.canvas = canvas;
        this.length = length;
        gc = this.canvas.getGraphicsContext2D();
        this.isHead = isHead;
        currentPosition = coordinate;
        isBig = false;
    }

    public void drawBodyPart(){
        if(isHead){
            gc.setFill(Color.GREEN);
        }else{
            gc.setFill(Color.RED);
        }
        if(isBig){
            gc.fillRect(currentPosition.getxCoord()-length,currentPosition.getyCoord()-length,length*3,length*3);
        }else{
            gc.fillRect(currentPosition.getxCoord()+1,currentPosition.getyCoord()+1,length-2,length-2);
        }

    }

    public void setCurrentPosition(Coordinate currentPosition){
        this.currentPosition = currentPosition;
    }

    public Coordinate getCurrentPosition(){
        return currentPosition;
    }





}
