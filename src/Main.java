import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;



public class Main extends Application {


    public static int height = 500;
    public static int width = 500;
    SnakeBody snakeBody;
    static int score;
    static Label scoreBoard;
    Label retry;
    static Rotate rotate = new Rotate();
    Label selectDifficulty;
    boolean isInsaneLevel;
    static RadioButton insaneDifficulty;
    static RadioButton easyDifficulty;
    double startTime;
    double currentTime;


    @Override
    public void start(Stage stage) throws Exception {

        //Initialisations
        startTime = System.currentTimeMillis();
        rotate = new Rotate();
        rotate.setPivotX(width/2);
        rotate.setPivotY(height/2);
        isInsaneLevel = false;

        Group root = new Group();
        AnchorPane anchorPane = new AnchorPane();

        anchorPane.setLayoutX(width/2 * (Math.sqrt(2)-1));
        anchorPane.setLayoutY(height/2 * (Math.sqrt(2)-1));


        //Initialise the nodes
        Canvas canvas = new Canvas(width,height);

        scoreBoard = new Label("Score: 0");
        scoreBoard.setLayoutX(50);
        scoreBoard.setLayoutY(700);
        scoreBoard.setFont(new Font(25));

        retry = new Label("Press Enter to play restart");
        retry.setLayoutX(50);
        retry.setLayoutY(725);
        retry.setFont(new Font(25));
        selectDifficulty = new Label("Select Difficulty");

        selectDifficulty.setLayoutX(50);
        selectDifficulty.setLayoutY(750);
        selectDifficulty.setFont(new Font(25));

        //Initialise the radiobuttons
        insaneDifficulty = new RadioButton("Insane Difficulty");
        insaneDifficulty.setFont(new Font(25));
        insaneDifficulty.setLayoutX(50);
        insaneDifficulty.setLayoutY(830);

        easyDifficulty = new RadioButton("Easy Difficulty");
        easyDifficulty.setFont(new Font(25));
        easyDifficulty.setLayoutX(50);
        easyDifficulty.setLayoutY(790);

        //Group up the radio buttons. Make them exclusive
        ToggleGroup toggleGroup = new ToggleGroup();
        insaneDifficulty.setToggleGroup(toggleGroup);
        easyDifficulty.setToggleGroup(toggleGroup);
        easyDifficulty.setSelected(true);

        //Disable both buttons key event listeners
        easyDifficulty.setFocusTraversable(false);
        insaneDifficulty.setFocusTraversable(false);

        //Add the canvas to the anchorpane
        //Then add the anchorpane to the root
        //And finally create the scene with the root
        anchorPane.getChildren().add(canvas);
        root.getChildren().add(anchorPane);
        root.getChildren().addAll(scoreBoard, retry, selectDifficulty, insaneDifficulty, easyDifficulty);


        //anchorPane.getTransforms().add(new Rotate(20));
        anchorPane.getTransforms().add(rotate);

        easyDifficulty.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                rotate.setAngle(0);
            }
        });


        //If more stuff is to be added, add it straight to the root.
        Scene scene = new Scene(root, width*Math.sqrt(2), height*Math.sqrt(2)+200);
        scene.setFill(Color.SKYBLUE);

        //Set the stage
        stage.setScene(scene);
        stage.show();
        stage.setTitle("Snake 2.0");




        snakeBody = new SnakeBody(canvas);
        snakeBody.snakeAnimation();
        snakeBody.playAnimation();


        //The input controls
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {


            @Override
            public void handle(KeyEvent keyEvent) {
                currentTime = System.currentTimeMillis();
                if(currentTime -startTime <= 100){ //If the snake is turned twice within the same animation frame, it turns on itself
                    return;
                }

                if(keyEvent.getCode() == KeyCode.UP){
                    if(!(snakeBody.getCurrentDirection() == Direction.DOWN)){
                        snakeBody.setDirection(Direction.UP);
                        startTime = System.currentTimeMillis();
                    }
                }else if(keyEvent.getCode() == KeyCode.DOWN){
                    if(!(snakeBody.getCurrentDirection() == Direction.UP)){
                        snakeBody.setDirection(Direction.DOWN);
                        startTime = System.currentTimeMillis();
                    }
                }else if(keyEvent.getCode() == KeyCode.RIGHT){
                    if(!(snakeBody.getCurrentDirection() == Direction.LEFT)){
                        snakeBody.setDirection(Direction.RIGHT);
                        startTime = System.currentTimeMillis();
                    }
                }else if(keyEvent.getCode() == KeyCode.LEFT){
                    if(!(snakeBody.getCurrentDirection() == Direction.RIGHT)){
                        snakeBody.setDirection(Direction.LEFT);
                        startTime = System.currentTimeMillis();
                    }
                }else if(keyEvent.getCode() == KeyCode.ENTER){
                    snakeBody.timeline.stop();
                    snakeBody = new SnakeBody(canvas);
                    snakeBody.snakeAnimation();
                    snakeBody.playAnimation();
                    rotate.setAngle(0);
                    score = 0;
                    scoreBoard.setText("Score: " + score);
                }
            }
        });
    }

    public static void incrementScore(){
        scoreBoard.setText("Score: " + ++score);
        if(insaneDifficulty.isSelected()){
            rotate.setAngle(score%360);
        }

    }


}
