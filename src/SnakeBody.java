import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import java.util.LinkedList;



public class SnakeBody {
    LinkedList<BodyPart> theSnakeBody;
    Canvas canvas;
    Timeline timeline;
    BodyPart snakeHead;
    GraphicsContext gc;
    Direction currentDirection;
    boolean gameOver;
    Food food;
    double startTime;
    double effectDuration;
    double currentTime;
    double speedUpstartTime;
    double largeHeadStartTime;
    double speedRatio;

    public SnakeBody(Canvas canvas){
        //initialisations
        speedRatio = 1.5;
        this.canvas = canvas;
        gc = canvas.getGraphicsContext2D();
        theSnakeBody = new LinkedList<>();
        timeline = new Timeline();
        gameOver = false;
        paintBackGround();
        effectDuration = 5000;
        startTime = System.currentTimeMillis();
        //Randomise the starting position
        int startingXPosition;
        int startingYPosition;
        int maxstartingX = Main.width/Coordinate.unitSize; //should be about 30 or 40
        int maxStartingY = Main.height/Coordinate.unitSize;
        startingXPosition = (int)(Math.random()*maxstartingX); //This is in the range 0-30 or 0-40
        startingYPosition = (int)(Math.random()*(maxStartingY-6) + 3); //From 3 to 27. Needs a buffer

        snakeHead = new BodyPart(this.canvas, new Coordinate(startingXPosition*Coordinate.unitSize,startingYPosition*Coordinate.unitSize), Coordinate.unitSize, true);
        currentDirection = Direction.DOWN;
        snakeHead.drawBodyPart();
        theSnakeBody.add(snakeHead);

        //Initialise a few tail pieces
        theSnakeBody.add(new BodyPart(this.canvas, new Coordinate(startingXPosition*Coordinate.unitSize,(startingYPosition-1)*Coordinate.unitSize), Coordinate.unitSize, false));
        theSnakeBody.add(new BodyPart(this.canvas, new Coordinate(startingXPosition*Coordinate.unitSize,(startingYPosition-2)*Coordinate.unitSize), Coordinate.unitSize, false));
        generateFood();

        //Timer for food expiration

    }

    public void snakeAnimation(){
        timeline.setAutoReverse(false);
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(100), new EventHandler<ActionEvent>() { //Set the speed here
            @Override
            public void handle(ActionEvent actionEvent) {
                theGame();
            }
        }));

    }

    public void paintBackGround(){
        gc.setFill(Color.CORNSILK);
        gc.fillRect(0,0, Main.width, Main.height);
    }

    public void playAnimation(){
        timeline.play();
    }

    public void theGame(){

        //Collision check here
        if(collisionCheck()){
            timeline.stop();
            return;
        }

        //Check if food or speedup expires
        currentTime = System.currentTimeMillis();
        if(currentTime-startTime >= effectDuration){ // food resets and special effects resets after 5 seconds
            generateFood();
            startTime = currentTime;
        }



        //Reset the rate
        if(currentTime-speedUpstartTime > effectDuration){
            timeline.setRate(1);
        }

        //Reset the headsize
        if(currentTime - largeHeadStartTime > effectDuration){
            theSnakeBody.get(0).isBig = false;
        }


        willEatFood();

        int unitSize = Coordinate.unitSize;
        gc.clearRect(0,0, Main.width, Main.height);
        paintBackGround();
        food.drawFood();
        food.drawLifeOfFood((currentTime-startTime)/effectDuration);
        //Set the new positions
        for (int i = theSnakeBody.size()-1; i > 0; i--) {
            theSnakeBody.get(i).setCurrentPosition(theSnakeBody.get(i-1).getCurrentPosition());
        }

        //Which direction to move the Snake. Turning just the head
        if(currentDirection == Direction.RIGHT){
            theSnakeBody.get(0).setCurrentPosition(new Coordinate(snakeHead.getCurrentPosition().getxCoord()+unitSize, snakeHead.getCurrentPosition().getyCoord()));
        }else if(currentDirection == Direction.LEFT){
            theSnakeBody.get(0).setCurrentPosition(new Coordinate(snakeHead.getCurrentPosition().getxCoord()-unitSize, snakeHead.getCurrentPosition().getyCoord()));
        }else if(currentDirection == Direction.DOWN){
            theSnakeBody.get(0).setCurrentPosition(new Coordinate(snakeHead.getCurrentPosition().getxCoord(), snakeHead.getCurrentPosition().getyCoord()+unitSize));
        }else if(currentDirection == Direction.UP){
            theSnakeBody.get(0).setCurrentPosition(new Coordinate(snakeHead.getCurrentPosition().getxCoord(), snakeHead.getCurrentPosition().getyCoord()-unitSize));
        }
        theSnakeBody.get(0).drawBodyPart(); //Draw the snake head after turning it

        //Draw the whole dail
        for (int i = 0; i < theSnakeBody.size(); i++) {
            theSnakeBody.get(i).drawBodyPart();
        }
    }



    public void setDirection(Direction newDirection){
        currentDirection = newDirection;
    }

    public Direction getCurrentDirection(){
        return currentDirection;
    }



    public boolean collisionCheck(){
        if(currentDirection == Direction.RIGHT){ //If it is going right, x out of bounds
            //First check if collision with the wall
            if(theSnakeBody.get(0).getCurrentPosition().getxCoord()+Coordinate.unitSize>= Main.width){
                gameOver = true;
                return true;
            }
            //Next is collision with itself


        }else if(currentDirection == Direction.LEFT){
            if(theSnakeBody.get(0).getCurrentPosition().getxCoord()-Coordinate.unitSize < 0){
                gameOver = true;
                return true;
            }
        }else if(currentDirection == Direction.DOWN){
            if(theSnakeBody.get(0).getCurrentPosition().getyCoord()+Coordinate.unitSize>= Main.height){
                gameOver = true;
                return true;
            }
        }else if(currentDirection == Direction.UP){
            if(theSnakeBody.get(0).getCurrentPosition().getyCoord()-Coordinate.unitSize< 0){
                gameOver = true;
                return true;
            }
        }
        for (int i = 1; i < theSnakeBody.size(); i++) {
            if(theSnakeBody.get(0).getCurrentPosition().getxCoord() == theSnakeBody.get(i).getCurrentPosition().getxCoord() && theSnakeBody.get(0).getCurrentPosition().getyCoord() == theSnakeBody.get(i).getCurrentPosition().getyCoord()){
                gameOver = true;
                return true;
            }
        }
        return false;
    }

    public void generateFood(){
        int rand = (int)(Math.random()*100); //generate a number between 0 and 99
        //if 0-19, speed food
        //if 20-39, large head
        //else normal nomal food

        int x = (int)(Math.random()*(Main.width/Coordinate.unitSize-10) + 5);
        int y = (int)(Math.random()*(Main.height/Coordinate.unitSize-10) + 5);
        boolean overlap = false;

        //Check if these coordinates are part of the snake

        do {
            for (BodyPart bodyPart : theSnakeBody) {
                if (bodyPart.getCurrentPosition().getxCoord() == x * Coordinate.unitSize && bodyPart.getCurrentPosition().getyCoord() == y * Coordinate.unitSize) {
                    System.out.println("Overlap");
                    x = (int) (Math.random() * (Main.width / Coordinate.unitSize - 10) + 5);
                    y = (int) (Math.random() * (Main.height / Coordinate.unitSize - 10) + 5);
                    overlap = true;
                }else{
                    overlap = false;
                }
            }
        }while(overlap);

        //Generate a certain type of food, depending on the random number
/*
        if(rand == 0){
            food = new Food(canvas, new Coordinate(x*Coordinate.unitSize,y*Coordinate.unitSize), FoodType.GROWTAIL);
        }else if(rand == 1){
            food = new Food(canvas, new Coordinate(x*Coordinate.unitSize,y*Coordinate.unitSize), FoodType.SPEEDUP);
        }else if(rand == 2){
            food = new Food(canvas, new Coordinate(x*Coordinate.unitSize,y*Coordinate.unitSize), FoodType.GROWHEAD);
        }


 */
        System.out.println(rand);
        if(rand < 20){
            food = new Food(canvas, new Coordinate(x*Coordinate.unitSize,y*Coordinate.unitSize), FoodType.SPEEDUP);
        }else if(rand < 39){
            food = new Food(canvas, new Coordinate(x*Coordinate.unitSize,y*Coordinate.unitSize), FoodType.GROWHEAD);
        }else{
            food = new Food(canvas, new Coordinate(x*Coordinate.unitSize,y*Coordinate.unitSize), FoodType.GROWTAIL);
        }
        food.drawFood();

    }


    public boolean willEatFood(){
        if(theSnakeBody.get(0).getCurrentPosition().getxCoord() == food.getPosition().getxCoord() && theSnakeBody.get(0).getCurrentPosition().getyCoord() == food.getPosition().getyCoord()){
            if(food.foodType == FoodType.GROWTAIL){
                theSnakeBody.addLast(new BodyPart(this.canvas, food.getPosition(), Coordinate.unitSize, false));
            }else if(food.foodType == FoodType.SPEEDUP){
                timeline.setRate(speedRatio);
                speedUpstartTime = System.currentTimeMillis();
            }else if(food.foodType == FoodType.GROWHEAD){
                theSnakeBody.get(0).isBig = true;
                largeHeadStartTime = System.currentTimeMillis();
            }
            startTime = System.currentTimeMillis();
            generateFood();
            Main.incrementScore();
            return true;
        }


        if(theSnakeBody.get(0).isBig){ //The special case when the head is larger
            if((food.getPosition().getxCoord() >= theSnakeBody.get(0).getCurrentPosition().getxCoord()-Coordinate.unitSize && food.getPosition().getxCoord() <= theSnakeBody.get(0).getCurrentPosition().getxCoord()+Coordinate.unitSize)){
                if(food.getPosition().getyCoord() >= theSnakeBody.get(0).getCurrentPosition().getyCoord()-Coordinate.unitSize && food.getPosition().getyCoord() <= theSnakeBody.get(0).getCurrentPosition().getyCoord()+Coordinate.unitSize){
                    if(food.foodType == FoodType.GROWTAIL){
                        theSnakeBody.addLast(new BodyPart(this.canvas, food.getPosition(), Coordinate.unitSize, false));
                    }else if(food.foodType == FoodType.SPEEDUP){
                        timeline.setRate(speedRatio);
                        speedUpstartTime = System.currentTimeMillis();
                    }else if(food.foodType == FoodType.GROWHEAD){
                        theSnakeBody.get(0).isBig = true;
                        largeHeadStartTime = System.currentTimeMillis();
                    }
                    startTime = System.currentTimeMillis();
                    generateFood();
                    Main.incrementScore();
                    return true;
                }
            }
        }
        return false;
    }


}
